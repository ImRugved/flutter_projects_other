
import 'package:get/get.dart';

class FirstScreenController extends GetxController {
  RxInt counter = 0.obs;
  void add() {
    counter++;
  }
}
