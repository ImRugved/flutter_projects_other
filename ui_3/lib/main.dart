import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:slider_button/slider_button.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Flutter Demo",
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          MyAppBar(),
          const SizedBox(
            height: 25,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  'File',
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.w900),
                ),
                IconButton(
                  onPressed: () {},
                  icon: const Icon(
                    Icons.tune,
                    size: 30,
                    color: Colors.black,
                  ),
                ),
              ],
            ),
          ),
          const Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Text(
                'Manager',
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.w400),
              ),
            ),
          ),
          const SizedBox(
            height: 15,
          ),
          const Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Text(
                "Lets's clean and manage your files",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
              ),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            margin: const EdgeInsets.only(left: 10),
            height: 230,
            child: Expanded(
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: [
                  MyCards(
                    mycolor: const Color(0xff22293d),
                    mycolor2: Color(0xff434e6c),
                  ),
                  MyCards(
                    mycolor: const Color(0xff3787eb),
                    mycolor2: Color(0xff1b70da),
                  ),
                  MyCards(
                    mycolor: const Color(0xffff5a00),
                    mycolor2: Color(0xffc84e0c),
                  ),
                  MyCards(
                    mycolor: const Color(0xff3787eb),
                    mycolor2: Color(0xff1b70da),
                  ),
                  MyCards(
                    mycolor: const Color(0xff22293d),
                    mycolor2: Color(0xff434e6c),
                  ),
                  MyCards(
                    mycolor: const Color(0xffff5a00),
                    mycolor2: Color(0xffc84e0c),
                  ),
                  MyCards(
                    mycolor: const Color(0xff3787eb),
                    mycolor2: Color(0xff1b70da),
                  ),
                  MyCards(
                    mycolor: const Color(0xff22293d),
                    mycolor2: Color(0xff434e6c),
                  ),
                  MyCards(
                    mycolor: const Color(0xffff5a00),
                    mycolor2: Color(0xffc84e0c),
                  ),
                  MyCards(
                    mycolor: const Color(0xff3787eb),
                    mycolor2: Color(0xff1b70da),
                  ),
                  MyCards(
                    mycolor: const Color(0xff22293d),
                    mycolor2: Color(0xff434e6c),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class MyCards extends StatelessWidget {
  Color mycolor;
  Color mycolor2;
  MyCards({required this.mycolor, required this.mycolor2});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 10),
      padding: const EdgeInsets.all(20),
      width: 180,
      decoration: BoxDecoration(
        color: mycolor,
        borderRadius: BorderRadiusDirectional.circular(25),
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Manager',
                style: TextStyle(color: mycolor2, fontSize: 15),
              ),
              const Icon(
                Icons.more_horiz,
                color: Colors.white,
              ),
            ],
          ),
          const SizedBox(
            height: 13,
          ),
          const Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Larger',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                    fontWeight: FontWeight.w600),
              ),
              Text(
                '92',
                style: TextStyle(
                  color: Color(0xffcfff00),
                  fontSize: 23,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Files',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                    fontWeight: FontWeight.w600),
              ),
              Text(
                'items',
                style: TextStyle(
                  color: mycolor2,
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 13,
          ),
          Container(
            height: 8,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: LinearProgressIndicator(
                value: 0.7,
                backgroundColor: mycolor2,
                valueColor: AlwaysStoppedAnimation<Color>(
                  Color(0xffcfff00),
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 3,
          ),
          const Align(
            alignment: Alignment.centerLeft,
            child: Text(
              '72.40GB / 128GB',
              style: TextStyle(color: Colors.white, fontSize: 9),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Expanded(
            child: Container(
              height: 40,
              child: SliderButton(
                action: () {
                  return Future.delayed(Duration());
                },
                label: Text(
                  '>>>',
                  style: TextStyle(fontSize: 18),
                ),
                backgroundColor: mycolor2,
                child: Container(
                  margin: const EdgeInsets.symmetric(vertical: 4),
                  height: double.infinity,
                  width: 60,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(18),
                  ),
                  child: Center(
                    child: Text(
                      'Clean',
                      style: TextStyle(color: mycolor2),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class MyAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        margin: const EdgeInsets.only(top: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.arrow_left,
                size: 40,
              ),
            ),
            Container(
              margin: const EdgeInsets.only(right: 30),
              height: 30,
              width: 30,
              decoration: BoxDecoration(
                color: Colors.blue,
                borderRadius: BorderRadius.circular(8),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
