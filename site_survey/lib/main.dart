import 'package:alleviz/screens/login/view/addressScreen.dart';
import 'package:alleviz/screens/login/view/categoryScreen.dart';
import 'package:alleviz/screens/login/view/login.dart';
import 'package:alleviz/screens/login/view/mediaScreen.dart';
import 'package:alleviz/screens/login/view/showDataScreen.dart';
import 'package:alleviz/screens/login/view/sitenamescreen.dart';

import 'package:alleviz/screens/login/view/surveryName.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(360, 690),
      minTextAdapt: true,
      splitScreenMode: false,
      builder: (context, child) {
        return const MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'Flutter Demo',
            home: SiteNameScreen());
      },
    );
  }
}
