// To parse this JSON data, do
//
//     final siteSurvey = siteSurveyFromJson(jsonString);

import 'dart:convert';

SiteSurvey siteSurveyFromJson(String str) =>
    SiteSurvey.fromJson(json.decode(str));

String siteSurveyToJson(SiteSurvey data) => json.encode(data.toJson());

class SiteSurvey {
  String? address;
  String? category;
  String? city;
  String? companyName;
  String? country;
  DateTime? createdOn;
  String? siteName;
  String? state;
  List<SurveyDetail>? surveyDetails;
  int? surveyId;
  String? surveyType;
  DateTime? updatedOn;

  SiteSurvey({
    this.address,
    this.category,
    this.city,
    this.companyName,
    this.country,
    this.createdOn,
    this.siteName,
    this.state,
    this.surveyDetails,
    this.surveyId,
    this.surveyType,
    this.updatedOn,
  });

  factory SiteSurvey.fromJson(Map<String, dynamic> json) => SiteSurvey(
        address: json["address"],
        category: json["category"],
        city: json["city"],
        companyName: json["companyName"],
        country: json["country"],
        createdOn: json["createdOn"] == null
            ? null
            : DateTime.parse(json["createdOn"]),
        siteName: json["siteName"],
        state: json["state"],
        surveyDetails: json["surveyDetails"] == null
            ? []
            : List<SurveyDetail>.from(
                json["surveyDetails"]!.map((x) => SurveyDetail.fromJson(x))),
        surveyId: json["surveyId"],
        surveyType: json["surveyType"],
        updatedOn: json["updatedOn"] == null
            ? null
            : DateTime.parse(json["updatedOn"]),
      );

  Map<String, dynamic> toJson() => {
        "address": address,
        "category": category,
        "city": city,
        "companyName": companyName,
        "country": country,
        "createdOn": createdOn?.toIso8601String(),
        "siteName": siteName,
        "state": state,
        "surveyDetails": surveyDetails == null
            ? []
            : List<dynamic>.from(surveyDetails!.map((x) => x.toJson())),
        "surveyId": surveyId,
        "surveyType": surveyType,
        "updatedOn": updatedOn?.toIso8601String(),
      };
}

class SurveyDetail {
  String? photoAndVideo;
  int? subSurveyId;

  SurveyDetail({
    this.photoAndVideo,
    this.subSurveyId,
  });

  factory SurveyDetail.fromJson(Map<String, dynamic> json) => SurveyDetail(
        photoAndVideo: json["photoAndVideo"],
        subSurveyId: json["subSurveyId"],
      );

  Map<String, dynamic> toJson() => {
        "photoAndVideo": photoAndVideo,
        "subSurveyId": subSurveyId,
      };
}
