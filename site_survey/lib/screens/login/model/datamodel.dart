class DataModel {
  String? siteName;
  String? companyName;
  String? category;
  String? surveyName;
  String? country;
  String? state;
  String? city;
  String? surveydes;
  List<String>? images;

  DataModel({
    this.siteName,
    this.companyName,
    this.category,
    this.surveyName,
    this.country,
    this.state,
    this.city,
    this.images,
    this.surveydes,
  });
}
