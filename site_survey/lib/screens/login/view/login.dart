// import 'dart:convert';
// import 'dart:developer';
// import 'dart:io';

// import 'package:alleviz/screens/login/model/datamodel.dart';
// import 'package:alleviz/screens/login/view/showDataScreen.dart';
// import 'package:dropdown_button2/dropdown_button2.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:image_picker/image_picker.dart';



// class FormScreen extends StatefulWidget {
//   const FormScreen({super.key});

//   @override
//   State<FormScreen> createState() => FormScreenState();
// }

// class FormScreenState extends State<FormScreen> {
//   List<String> imageList = [];

//   List<DataModel> dataList = [
//     DataModel(
//       sitename: 'site',
//       companyName: 'Daccess',
//       surveyName: 'survey',
//       countryName: 'india',
//       stateName: 'maharashtra',
//       cityName: 'pune',
//       mediaName: ''
//     ),
    
//   ];
//   get() async {
//     ImagePicker picker = ImagePicker();
//     final pickedFile =
//         await picker.pickMultipleMedia(maxHeight: 150, maxWidth: 150);

//     if (pickedFile.first.path != null) {
//       File pickedVideo = File(pickedFile.first.path);
//       String credentials =
//           pickedFile.first.path;
//       Codec<String, String> stringToBase64 = utf8.fuse(base64);
//       String encoded = stringToBase64.encode(credentials);
//       //log("file is ${pickedFile.first.path}");
//       log("encodedeis ${encoded}");
//       for (int i = 0; i <= pickedFile.length; i++) {
//         imageList.add(pickedFile[i].path);
//          log(imageList[i]);
//       }

     
//     }
//   }

//   void addData() {
//     String siteNm = sitenameController.text.trim();
//     String compNm = companynameController.text.trim();

//     String survey = surveyController.text.trim();
//     String country = countryController.text;
//     String state = stateController.text;
//     String city = cityController.text;
//     String media = mediaController.text;
//     if (siteNm.isNotEmpty &&
//         compNm.isNotEmpty &&
//         survey.isNotEmpty &&
//         country.isNotEmpty &&
//         state.isNotEmpty &&
//         city.isNotEmpty&&media.isNotEmpty) {
//       dataList.add(
//         DataModel(
//           sitename: siteNm,
//           companyName: compNm,
//           surveyName: survey,
//           countryName: country,
//           stateName: state,
//           cityName: city,
//           mediaName: media,
//         ),
//       );
//     }
//     print(siteNm);
//   }

//   TextEditingController sitenameController = TextEditingController();
//   TextEditingController companynameController = TextEditingController();
//   TextEditingController categoryController = TextEditingController();
//   TextEditingController surveyController = TextEditingController();
//   TextEditingController countryController = TextEditingController();
//   TextEditingController stateController = TextEditingController();
//   TextEditingController cityController = TextEditingController();
//   TextEditingController mediaController = TextEditingController();

//   final List<String> Countryitems = [
//     'India',
//     'China',
//     'USA',
//     'England',
//   ];

//   String? selectedCountry;

//   final List<String> Stateitems = [
//     'Maharashtra',
//     'Gujrat',
//     'Assam',
//     'Karnataka',
//   ];

//   String? selectedState;

//   final List<String> Cityitems = [
//     'Latur',
//     'Pune',
//     'Nashik',
//     'Nagpur',
//   ];

//   String? selectedCity;

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.purple[300],
//         centerTitle: true,
//         title: const Text(
//           'Site Suevey',
//         ),
//       ),
//       body: SingleChildScrollView(
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.center,
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: [
//             textField(sitenameController, 'Site Name'),
//             const SizedBox(
//               height: 10,
//             ),
//             textField(companynameController, 'Company Name'),
//             const SizedBox(
//               height: 10,
//             ),
//             textField(surveyController, 'Survey'),
//             const SizedBox(
//               height: 10,
//             ),
//             Row(
//               mainAxisAlignment: MainAxisAlignment.start,
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 const SizedBox(
//                   width: 20,
//                 ),
//                 Text('Address : '),
//                 const SizedBox(
//                   width: 20,
//                 ),
//                 Column(
//                   children: [
//                     DropdownButtonHideUnderline(
//                       child: DropdownButton2<String>(
//                         isExpanded: true,
//                         hint: Text(
//                           'Select Country',
//                           style: TextStyle(
//                             fontSize: 14,
//                             color: Theme.of(context).hintColor,
//                           ),
//                         ),
//                         items: Countryitems.map(
//                             (String item) => DropdownMenuItem<String>(
//                                   value: item,
//                                   child: Text(
//                                     item,
//                                     style: const TextStyle(
//                                       fontSize: 14,
//                                     ),
//                                   ),
//                                 )).toList(),
//                         value: selectedCountry,
//                         onChanged: (String? value) {
//                           setState(() {
//                             selectedCountry = value;
//                           });
//                         },
//                         buttonStyleData: const ButtonStyleData(
//                           padding: EdgeInsets.symmetric(horizontal: 16),
//                           height: 40,
//                           width: 180,
//                           decoration: BoxDecoration(
//                             borderRadius: BorderRadius.all(
//                               Radius.circular(
//                                 10,
//                               ),
//                             ),
//                           ),
//                         ),
//                         menuItemStyleData: const MenuItemStyleData(
//                           height: 40,
//                         ),
//                       ),
//                     ),
//                     const SizedBox(
//                       height: 10,
//                     ),
//                     DropdownButtonHideUnderline(
//                       child: DropdownButton2<String>(
//                         isExpanded: true,
//                         hint: Text(
//                           'Select State',
//                           style: TextStyle(
//                             fontSize: 14,
//                             color: Theme.of(context).hintColor,
//                           ),
//                         ),
//                         items: Stateitems.map(
//                             (String item) => DropdownMenuItem<String>(
//                                   value: item,
//                                   child: Text(
//                                     item,
//                                     style: const TextStyle(
//                                       fontSize: 14,
//                                     ),
//                                   ),
//                                 )).toList(),
//                         value: selectedState,
//                         onChanged: (String? value) {
//                           setState(() {
//                             selectedState = value;
//                           });
//                         },
//                         buttonStyleData: const ButtonStyleData(
//                           padding: EdgeInsets.symmetric(horizontal: 16),
//                           height: 40,
//                           width: 140,
//                           decoration: BoxDecoration(
//                             borderRadius: BorderRadius.all(
//                               Radius.circular(
//                                 10,
//                               ),
//                             ),
//                           ),
//                         ),
//                         menuItemStyleData: const MenuItemStyleData(
//                           height: 40,
//                         ),
//                       ),
//                     ),
//                     const SizedBox(
//                       height: 10,
//                     ),
//                     DropdownButtonHideUnderline(
//                       child: DropdownButton2<String>(
//                         isExpanded: true,
//                         hint: Text(
//                           'Select City',
//                           style: TextStyle(
//                             fontSize: 14,
//                             color: Theme.of(context).hintColor,
//                           ),
//                         ),
//                         items: Cityitems.map(
//                             (String item) => DropdownMenuItem<String>(
//                                   value: item,
//                                   child: Text(
//                                     item,
//                                     style: const TextStyle(
//                                       fontSize: 14,
//                                     ),
//                                   ),
//                                 )).toList(),
//                         value: selectedCity,
//                         onChanged: (String? value) {
//                           setState(() {
//                             selectedCity = value;
//                           });
//                         },
//                         buttonStyleData: const ButtonStyleData(
//                           padding: EdgeInsets.symmetric(horizontal: 16),
//                           height: 40,
//                           width: 140,
//                           decoration: BoxDecoration(
//                             borderRadius: BorderRadius.all(
//                               Radius.circular(
//                                 10,
//                               ),
//                             ),
//                           ),
//                         ),
//                         menuItemStyleData: const MenuItemStyleData(
//                           height: 40,
//                         ),
//                       ),
//                     ),
//                   ],
//                 )
//               ],
//             ),
//             const SizedBox(
//               height: 10,
//             ),
//             SizedBox(
//               height: 50,
//               width: 200,
//               child: ElevatedButton(
//                 onPressed: () {
//                   get();
//                 },
//                 child: Text('Select Photo/Video'),
//               ),
//             ),
//             const SizedBox(
//               height: 10,
//             ),
//             ElevatedButton(
//               onPressed: () {
//                 setState(() {
//                   addData();
//                   Navigator.of(context).push(
//                     MaterialPageRoute(
//                       builder: (context) => ShowDataScreen(),
//                     ),
//                   );
//                 });
//               },
//               child: Text('Save'),
//             ),
//           ],
//         ),
//       ),
//     );
//   }

//   Widget textField(TextEditingController cntrl, String labelText) {
//     return SingleChildScrollView(
//       child: Container(
//         padding: EdgeInsets.all(10),
//         child: TextFormField(
//           controller: cntrl,
//           decoration: InputDecoration(
//             contentPadding: EdgeInsets.all(15),
//             border: OutlineInputBorder(
//               borderRadius: BorderRadius.circular(
//                 20,
//               ),
//             ),
//             labelText: labelText,
//           ),
//         ),
//       ),
//     );
//   }
// }
