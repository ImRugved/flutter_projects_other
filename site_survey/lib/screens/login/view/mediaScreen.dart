import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:alleviz/screens/login/model/datamodel.dart';
import 'package:alleviz/screens/login/view/addressScreen.dart';
import 'package:alleviz/screens/login/view/categoryScreen.dart';
import 'package:alleviz/screens/login/view/companyName.dart';
import 'package:alleviz/screens/login/view/showDataScreen.dart';
import 'package:alleviz/screens/login/view/sitenamescreen.dart';
import 'package:alleviz/screens/login/view/surveryName.dart';
import 'package:alleviz/screens/login/view/surveydescription.dart';

import 'package:flutter/material.dart';

import 'package:image_picker/image_picker.dart';

TextEditingController mediaController = TextEditingController();

class MediaScreen extends StatefulWidget {
  const MediaScreen({Key? key}) : super(key: key);

  @override
  State<MediaScreen> createState() => _MediaScreenState();
}

class _MediaScreenState extends State<MediaScreen> {
  List<String> imageList = [];

  List<DataModel> dataList = [];

  List<String> selectedImg = [];
  List<File> pickImages = [];

  Future<void> get() async {
    ImagePicker picker = ImagePicker();
    final pickedFiles = await picker.pickMultipleMedia();

    for (int i = 0; i < pickedFiles.length; i++) {
      // pickedFiles.add(pickedFiles[i]);
      String credentials = pickedFiles[i].path;
      Codec<String, String> stringToBase64 = utf8.fuse(base64);
      String encoded = stringToBase64.encode(credentials);
      log("file is ${pickedFiles.first.path}");
      log("encodedeis ${encoded}");
      setState(() {
        selectedImg.add(encoded);
      });
    }
  }

  void addData() {
    setState(() {
      String? siteNm = sitenameController.text.trim();
      String? compNm = companynameController.text.trim();
      String? catgry = selectedCategory!;
      String? survy = selectedSurvey!;
      String? cntry = selectedCountry!;
      String? state = selectedState!;
      String? city = selectedCity!;
      String? surdes = surveydescController.text.trim();

      dataList.add(DataModel(
        siteName: siteNm,
        companyName: compNm,
        category: catgry,
        surveyName: survy,
        country: cntry,
        state: state,
        city: city,
        surveydes: surdes,
        images: selectedImg,
      ));
      log(dataList.toString());
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => ShowDataScreen(
            dataList: dataList,
            list: selectedImg,
          ),
        ),
      );
    });
    sitenameController.clear();
    companynameController.clear();
    print('desc is : ${surveydescController.text}');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(245, 245, 247, 1),
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: const Color(0xFFBA68C8),
        centerTitle: true,
        title: const Text(
          'Media Screen',
          style: TextStyle(
              color: Color.fromRGBO(245, 245, 247, 1),
              fontWeight: FontWeight.w500),
        ),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.all(10),
                decoration: const BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black87,
                      offset: Offset(4, 4),
                      blurRadius: 25,
                    )
                  ],
                ),
                height: 300,
                width: 400,
                child: Card(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const SizedBox(
                        height: 30,
                      ),
                      SizedBox(
                        height: 50,
                        width: 150,
                        child: ElevatedButton(
                          onPressed: () {
                            get();
                            setState(() {});
                          },
                          child: const Text('Pick Images'),
                        ),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16, right: 16),
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              (pickImages.isEmpty)
                                  ? Text('No media selected')
                                  : Text(pickImages[0].path.split("/").last),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 50,
                    width: 150,
                    child: ElevatedButton(
                      onPressed: () {
                        setState(() {
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) => const SurveyDescScreen(),
                            ),
                          );
                        });
                      },
                      child: const Text('Back'),
                    ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  SizedBox(
                    height: 50,
                    width: 150,
                    child: ElevatedButton(
                      style: const ButtonStyle(
                        backgroundColor: MaterialStatePropertyAll(
                          Color(0xFFBA68C8),
                        ),
                      ),
                      onPressed: () {
                        addData();
                      },
                      child: const Text(
                        'Save',
                        style: TextStyle(
                          color: Color.fromRGBO(245, 245, 247, 1),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
