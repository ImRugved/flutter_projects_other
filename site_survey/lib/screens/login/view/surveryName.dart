import 'package:alleviz/screens/login/view/addressScreen.dart';
import 'package:alleviz/screens/login/view/categoryScreen.dart';
import 'package:flutter/material.dart';

String? selectedSurvey;

class SurveyNameScreen extends StatefulWidget {
  const SurveyNameScreen({Key? key}) : super(key: key);

  @override
  State<SurveyNameScreen> createState() => _SurveyNameScreenState();
}

class _SurveyNameScreenState extends State<SurveyNameScreen> {
  final List<String> Surveyitems = [
    'Pre Sale Survey',
    'Post Sale Survey',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.purple[300],
        centerTitle: true,
        title: const Text(
          'Survey Name',
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            decoration: const BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.black87,
                  offset: Offset(4, 4),
                  blurRadius: 25,
                )
              ],
            ),
            height: 300,
            width: 400,
            margin: EdgeInsets.only(right: 45, left: 45),
            child: Card(
              child: DropdownButtonHideUnderline(
                child: DropdownButton<String>(
                  isExpanded: true,
                  hint: Text(
                    'select survey',
                    style: TextStyle(
                      fontSize: 14,
                      color: Theme.of(context).hintColor,
                    ),
                  ),
                  items:
                      Surveyitems.map((String item) => DropdownMenuItem<String>(
                            value: item,
                            child: Center(
                              child: Text(
                                item,
                                style: const TextStyle(
                                  fontSize: 20,
                                ),
                              ),
                            ),
                          )).toList(),
                  value: selectedSurvey,
                  onChanged: (String? value) {
                    setState(() {
                      selectedSurvey = value;
                    });
                  },
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 15,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: 50,
                width: 150,
                child: ElevatedButton(
                  onPressed: () {
                    setState(() {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => const CategoryScreen(),
                        ),
                      );
                    });
                  },
                  child: const Text('Back'),
                ),
              ),
              const SizedBox(
                width: 15,
              ),
              SizedBox(
                height: 50,
                width: 150,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(
                      Color(0xFFBA68C8),
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => const AddressScreen(),
                        ),
                      );
                    });
                  },
                  child: const Text(
                    'Save',
                    style: TextStyle(
                      color: Color.fromRGBO(245, 245, 247, 1),
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
