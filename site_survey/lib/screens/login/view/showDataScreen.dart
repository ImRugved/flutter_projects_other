import 'dart:convert';
import 'dart:developer';

import 'package:alleviz/screens/login/model/datamodel.dart';
import 'package:alleviz/screens/login/view/addressScreen.dart';
import 'package:alleviz/screens/login/view/categoryScreen.dart';
import 'package:alleviz/screens/login/view/companyName.dart';
import 'package:alleviz/screens/login/view/sitenamescreen.dart';
import 'package:alleviz/screens/login/view/surveryName.dart';
import 'package:alleviz/screens/login/view/surveydescription.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ShowDataScreen extends StatefulWidget {
  final List<DataModel> dataList;
  final List<String> list;

  const ShowDataScreen({
    Key? key,
    required this.dataList,
    required this.list,
  }) : super(key: key);

  @override
  State<ShowDataScreen> createState() => _ShowDataScreenState();
}

List<SurveyDetail> surveyDetails = [];

class _ShowDataScreenState extends State<ShowDataScreen> {
  Future<void> sendPostRequest(
    String siteName,
    String companyName,
    String category,
    String country,
    String state,
    String city,
    List<String> encodeed,
  ) async {
    if (encodeed.isNotEmpty) {
      for (int a = 0; a < encodeed.length; a++) {
        surveyDetails
            .add(SurveyDetail(photoAndVideo: encodeed[a], subSurveyId: 0));
        log(surveyDetails[a].photoAndVideo);
      }
    }

    List<Map<String, dynamic>> coursesjson =
        surveyDetails.map((cycle) => cycle.toJson()).toList();
    print("cycleJsonList $coursesjson");
    final url = Uri.parse('http://192.168.10.66:8088/createSurvey');
    final headers = {'Content-Type': 'application/json'};
    final postData = {
      "category": category,
      "city": city,
      "companyName": companyName,
      "country": country,
      "siteName": siteName,
      "state": state,
      "surveyDetails": coursesjson
    };

    final response = await http.post(
      url,
      headers: headers,
      body: jsonEncode(postData),
    );

    if (response.statusCode == 200) {
    } else {
      print('Error is: $response');
    }
  }

  List pass = [];
  @override
  void initState() {
    pass = widget.list;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFFBA68C8),
        title: const Text(
          'Show Data',
          style: TextStyle(
            color: Color.fromRGBO(245, 245, 247, 1),
            fontWeight: FontWeight.w600,
            fontSize: 25,
          ),
        ),
      ),
      body: (widget.dataList.isEmpty)
          ? Center(
              child: Text('No Data To Show'),
            )
          : ListView.builder(
              itemCount: widget.dataList.length,
              itemBuilder: (context, index) => Column(
                children: [
                  Card(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.all(10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                  'Site Name: ${widget.dataList[index].siteName}'),
                              const SizedBox(height: 10),
                              Text(
                                  'Company Name: ${widget.dataList[index].companyName}'),
                              const SizedBox(height: 10),
                              Text(
                                  'Category: ${widget.dataList[index].category}'),
                              const SizedBox(height: 10),
                              Text(
                                  'Survey Name: ${widget.dataList[index].surveyName}'),
                              const SizedBox(height: 10),
                              Text(
                                  'Country: ${widget.dataList[index].country}'),
                              const SizedBox(height: 10),
                              Text('State: ${widget.dataList[index].state}'),
                              const SizedBox(height: 10),
                              Text('City: ${widget.dataList[index].city}'),
                              const SizedBox(height: 10),
                              Text(
                                  'Survey Description: ${widget.dataList[index].surveydes}'),
                              const SizedBox(height: 10),
                              Text('Media Files:'),
                              Column(
                                children: [Text(widget.list.first)],
                              ),
                              const SizedBox(
                                height: 30,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Center(
                        child: ElevatedButton(
                          onPressed: () {
                            setState(() {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) => SiteNameScreen(),
                                ),
                              );
                              sitenameController.clear();
                              companynameController.clear();
                              surveydescController.clear();
                            });
                          },
                          child: Text('Add New Data '),
                        ),
                      ),
                      const SizedBox(
                        width: 15,
                      ),
                      Center(
                        child: ElevatedButton(
                          style: const ButtonStyle(
                              backgroundColor: MaterialStatePropertyAll(
                            Color(0xFFBA68C8),
                          )),
                          onPressed: () async {
                            String siteName = sitenameController.text;
                            String companyName = companynameController.text;
                            String category = selectedCategory.toString();
                            String surveyDetails = selectedSurvey.toString();
                            String country = selectedCountry.toString();
                            String state = selectedState.toString();
                            String city = selectedCity.toString();
                            String survdec = surveydescController.toString();

                            await sendPostRequest(
                              siteName,
                              companyName,
                              category,
                              country,
                              state,
                              city,
                              widget.list,
                            );
                          },
                          child: Text(
                            'Post Data ',
                            style: TextStyle(
                              color: Color.fromRGBO(245, 245, 247, 1),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
    );
  }
}

class SurveyDetail {
  String photoAndVideo;
  int subSurveyId;

  SurveyDetail({required this.photoAndVideo, required this.subSurveyId});

  factory SurveyDetail.fromJson(Map<String, dynamic> json) {
    return SurveyDetail(
      photoAndVideo: json['photoAndVideo'],
      subSurveyId: json['subSurveyId'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'photoAndVideo': photoAndVideo,
      'subSurveyId': subSurveyId,
    };
  }
}
