import 'package:alleviz/screens/login/view/mediaScreen.dart';
import 'package:alleviz/screens/login/view/surveryName.dart';
import 'package:alleviz/screens/login/view/surveyDescription.dart';
import 'package:flutter/material.dart';

String? selectedCountry;
String? selectedState;
String? selectedCity;

class AddressScreen extends StatefulWidget {
  const AddressScreen({Key? key}) : super(key: key);

  @override
  State<AddressScreen> createState() => _AddressScreenState();
}

class _AddressScreenState extends State<AddressScreen> {
  final List<String> Countryitems = [
    'India',
    'China',
    'USA',
    'England',
  ];

  final List<String> Stateitems = [
    'Maharashtra',
    'Gujrat',
    'Assam',
    'Karnataka',
  ];

  final List<String> Cityitems = [
    'Latur',
    'Pune',
    'Nashik',
    'Nagpur',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Color(0xFFBA68C8),
        centerTitle: true,
        title: const Text(
          'Address',
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Container(
              decoration: const BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.black87,
                    offset: Offset(4, 4),
                    blurRadius: 25,
                  )
                ],
              ),
              child: Card(
                child: Container(
                  height: 300,
                  width: 400,
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      DropdownButtonHideUnderline(
                        child: DropdownButton<String>(
                          isExpanded: true,
                          hint: Text(
                            'Select Country',
                            style: TextStyle(
                              fontSize: 15,
                              color: Theme.of(context).hintColor,
                            ),
                          ),
                          items: Countryitems.map(
                              (String item) => DropdownMenuItem<String>(
                                    value: item,
                                    child: Center(
                                      child: Text(
                                        item,
                                        style: const TextStyle(
                                          fontSize: 22,
                                        ),
                                      ),
                                    ),
                                  )).toList(),
                          value: selectedCountry,
                          onChanged: (String? value) {
                            setState(() {
                              selectedCountry = value;
                            });
                          },
                        ),
                      ),
                      DropdownButtonHideUnderline(
                        child: DropdownButton<String>(
                          isExpanded: true,
                          hint: Text(
                            'Select State',
                            style: TextStyle(
                              fontSize: 14,
                              color: Theme.of(context).hintColor,
                            ),
                          ),
                          items: Stateitems.map(
                              (String item) => DropdownMenuItem<String>(
                                    value: item,
                                    child: Center(
                                      child: Text(
                                        item,
                                        style: const TextStyle(
                                          fontSize: 22,
                                        ),
                                      ),
                                    ),
                                  )).toList(),
                          value: selectedState,
                          onChanged: (String? value) {
                            setState(() {
                              selectedState = value;
                            });
                          },
                        ),
                      ),
                      DropdownButtonHideUnderline(
                        child: DropdownButton<String>(
                          isExpanded: true,
                          hint: Text(
                            'Select City',
                            style: TextStyle(
                              fontSize: 14,
                              color: Theme.of(context).hintColor,
                            ),
                          ),
                          items: Cityitems.map(
                              (String item) => DropdownMenuItem<String>(
                                    value: item,
                                    child: Center(
                                      child: Text(
                                        item,
                                        style: const TextStyle(
                                          fontSize: 22,
                                        ),
                                      ),
                                    ),
                                  )).toList(),
                          value: selectedCity,
                          onChanged: (String? value) {
                            setState(() {
                              selectedCity = value;
                            });
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          SizedBox(height: 30),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: 50,
                width: 150,
                child: ElevatedButton(
                  onPressed: () {
                    setState(() {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => const SurveyNameScreen(),
                        ),
                      );
                    });
                  },
                  child: const Text(
                    'Back',
                    style: TextStyle(fontSize: 20),
                  ),
                ),
              ),
              SizedBox(width: 20),
              SizedBox(
                height: 50,
                width: 150,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(
                      Color(0xFFBA68C8),
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => const SurveyDescScreen(),
                        ),
                      );
                    });
                  },
                  child: const Text(
                    'Save',
                    style: TextStyle(
                      color: Color.fromRGBO(245, 245, 247, 1),
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
