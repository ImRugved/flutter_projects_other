import 'package:alleviz/screens/login/view/companyName.dart';
import 'package:flutter/material.dart';

TextEditingController sitenameController = TextEditingController();

class SiteNameScreen extends StatefulWidget {
  const SiteNameScreen({Key? key}) : super(key: key);

  @override
  State<SiteNameScreen> createState() => _SiteNameScreenState();
}

class _SiteNameScreenState extends State<SiteNameScreen> {
  final GlobalKey<FormState> siteform = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.purple[300],
        centerTitle: true,
        title: const Text(
          'Site Name',
        ),
      ),
      body: SizedBox(
        height: double.infinity,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 300,
              width: 400,
              decoration: const BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.black87,
                    offset: Offset(4, 4),
                    blurRadius: 25,
                  )
                ],
              ),
              child: Card(
                margin: EdgeInsets.all(10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const SizedBox(
                      height: 15,
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 10, right: 10),
                      child: Form(
                        key: siteform,
                        child: TextFormField(
                          controller: sitenameController,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(
                                30,
                              ),
                            ),
                            contentPadding: const EdgeInsets.all(20),
                            labelText: ' Enter site name',
                            labelStyle: const TextStyle(
                              color: Color(0xFFBA68C8),
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return "Please enter site name";
                            }
                          },
                          onChanged: (value) {
                            setState(() {
                              value = sitenameController.text;
                            });
                          },
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 25,
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 50,
                  width: 100,
                  child: ElevatedButton(
                    style: const ButtonStyle(
                      backgroundColor: MaterialStatePropertyAll(
                        Color(0xFFBA68C8),
                      ),
                    ),
                    onPressed: () {
                      setState(() {
                        if (siteform.currentState!.validate()) {
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) => const CompanyNameScreen(),
                            ),
                          );
                        } else {
                          return;
                        }
                      });
                    },
                    child: const Text(
                      'Save',
                      style: TextStyle(
                        color: Color.fromRGBO(245, 245, 247, 1),
                        fontSize: 20,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
