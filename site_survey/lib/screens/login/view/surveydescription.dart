import 'package:alleviz/screens/login/view/addressScreen.dart';
import 'package:alleviz/screens/login/view/categoryScreen.dart';
import 'package:alleviz/screens/login/view/mediaScreen.dart';
import 'package:alleviz/screens/login/view/sitenamescreen.dart';
import 'package:flutter/material.dart';

TextEditingController surveydescController = TextEditingController();

class SurveyDescScreen extends StatefulWidget {
  const SurveyDescScreen({Key? key}) : super(key: key);

  @override
  State<SurveyDescScreen> createState() => _SurveyDescScreenState();
}

class _SurveyDescScreenState extends State<SurveyDescScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.purple[300],
        centerTitle: true,
        title: const Text(
          'Survey Description',
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: 300,
            width: 400,
            decoration: const BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.black87,
                  offset: Offset(4, 4),
                  blurRadius: 25,
                )
              ],
            ),
            child: Card(
              margin: EdgeInsets.all(10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const SizedBox(
                    height: 15,
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 10, right: 10),
                    child: TextFormField(
                      controller: surveydescController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(
                            30,
                          ),
                        ),
                        contentPadding: const EdgeInsets.all(20),
                        labelText: 'enter survey description',
                        labelStyle: const TextStyle(
                          color: Color(0xFFBA68C8),
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(
            height: 25,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: 50,
                width: 150,
                child: ElevatedButton(
                  onPressed: () {
                    setState(() {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => const AddressScreen(),
                        ),
                      );
                    });
                    print('desc is :${surveydescController.text}');
                  },
                  child: const Text('Back'),
                ),
              ),
              const SizedBox(
                width: 15,
              ),
              SizedBox(
                height: 50,
                width: 150,
                child: ElevatedButton(
                  style: const ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(
                      Color(0xFFBA68C8),
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      if (surveydescController.text.trim().isNotEmpty) {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => const MediaScreen(),
                          ),
                        );
                      } else {
                        return;
                      }
                    });
                  },
                  child: const Text(
                    'Save',
                    style: TextStyle(
                      color: Color.fromRGBO(245, 245, 247, 1),
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
