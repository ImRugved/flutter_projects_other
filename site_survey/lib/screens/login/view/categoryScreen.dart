import 'package:alleviz/screens/login/view/companyName.dart';
import 'package:alleviz/screens/login/view/surveryName.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

String? selectedCategory;

class CategoryScreen extends StatefulWidget {
  const CategoryScreen({Key? key}) : super(key: key);

  @override
  State<CategoryScreen> createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> {
  final List<String> Categoryitems = [
    'IT',
    'Education',
    'Medical',
    'Core',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Color(0xFFBA68C8),
        centerTitle: true,
        title: const Text(
          'Category Screen',
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: 300,
            width: 400,
            decoration: const BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.black87,
                  offset: Offset(4, 4),
                  blurRadius: 25,
                )
              ],
            ),
            margin: EdgeInsets.only(right: 20, left: 20),
            child: SizedBox(
              child: Card(
                child: DropdownButtonHideUnderline(
                  child: DropdownButton<String>(
                    isExpanded: true,
                    hint: Text(
                      'Select Category',
                      style: TextStyle(
                        fontSize: 14,
                        color: Theme.of(context).hintColor,
                      ),
                    ),
                    items: Categoryitems.map(
                        (String item) => DropdownMenuItem<String>(
                              value: item,
                              child: Center(
                                child: Text(
                                  item,
                                  style: const TextStyle(
                                    fontSize: 20,
                                  ),
                                ),
                              ),
                            )).toList(),
                    value: selectedCategory,
                    onChanged: (String? value) {
                      setState(() {
                        selectedCategory = value;
                      });
                    },
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 15,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                onPressed: () {
                  setState(() {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => const CompanyNameScreen(),
                      ),
                    );
                  });
                },
                child: const Text('Back'),
              ),
              const SizedBox(
                width: 15,
              ),
              ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStatePropertyAll(
                    Color(0xFFBA68C8),
                  ),
                ),
                onPressed: () {
                  setState(() {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => const SurveyNameScreen(),
                      ),
                    );
                  });
                },
                child: const Text(
                  'Save',
                  style: TextStyle(
                    color: Color.fromRGBO(245, 245, 247, 1),
                    fontSize: 20,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
