import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var n1Controller = TextEditingController();
  var n2Controller = TextEditingController();
  var result = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amber,
        title: Text('Calculator'),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                controller: n1Controller,
                keyboardType: TextInputType.number,
              ),
              const SizedBox(
                height: 20,
              ),
              TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                controller: n2Controller,
                keyboardType: TextInputType.number,
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    onPressed: () {
                      var n1 = int.parse(n1Controller.text.toString());
                      var n2 = int.parse(n2Controller.text.toString());
                      var sum = n1 + n2;
                      result = "The sum of $n1 and $n2 is $sum";
                      setState(() {});
                    },
                    child: Text('Add'),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  ElevatedButton(
                    onPressed: () {
                      var n1 = int.parse(n1Controller.text.toString());
                      var n2 = int.parse(n2Controller.text.toString());
                      var sum = n1 - n2;
                      result = "The subtraction of $n1 and $n2 is $sum";
                      setState(() {});
                    },
                    child: Text('Sub'),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  ElevatedButton(
                    onPressed: () {
                      var n1 = int.parse(n1Controller.text.toString());
                      var n2 = int.parse(n2Controller.text.toString());
                      var sum = n1 * n2;
                      result = "The multiplication of $n1 and $n2 is $sum";
                      setState(() {});
                    },
                    child: Text('Multiply'),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  ElevatedButton(
                    onPressed: () {
                      var n1 = int.parse(n1Controller.text.toString());
                      var n2 = int.parse(n2Controller.text.toString());
                      var sum = n1 / n2;
                      result =
                          "The division of $n1 and $n2 is ${sum.remainder(2)}";
                      setState(() {});
                    },
                    child: Text('Divide'),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                ],
              ),
              const SizedBox(
                height: 30,
              ),
              Text('$result')
            ],
          ),
        ),
      ),
    );
  }
}
