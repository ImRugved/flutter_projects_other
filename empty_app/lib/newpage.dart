import 'package:flutter/material.dart';

class NewPage extends StatelessWidget {
  const NewPage({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.purple,
        appBar: AppBar(
          title: const Text('New page'),
        ),
        body: Center(
          child: Container(
            height: 200,
            width: 200,
            color: Colors.black,
            child: Center(
              child: ElevatedButton(
                  style: ButtonStyle(),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text('back to home page')),
            ),
          ),
        ),
      ),
    );
  }
}
