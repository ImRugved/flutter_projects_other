import 'package:flutter/material.dart';
import 'package:calculator_app/app/views/calculator_view.dart';

class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Colors.lightBlue,
      body: SafeArea(
        child: CalculatorView(),
      ),
    );
  }
}
