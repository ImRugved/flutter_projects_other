import 'package:flutter/material.dart';
import 'package:calculator_app/app/views/home_view.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Flutter Learn",
      home: HomeView(),
    );
  }
}
