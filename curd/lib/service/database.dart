import 'package:cloud_firestore/cloud_firestore.dart';

class DatabaseMethods {
  Future<void> addEmployeeDetails(
      Map<String, dynamic> employeeInfoMap, String id) async {
    try {
      await FirebaseFirestore.instance
          .collection('Employee')
          .doc(id)
          .set(employeeInfoMap);
      print('Employee details added successfully.');
    } catch (e) {
      print('Error adding employee details: $e');
      // Handle the error as needed
      // You might want to throw an exception or notify the user
      rethrow; // Rethrow the caught exception for further handling if necessary
    }
  }

  Future<Stream<QuerySnapshot>> getEmployeeDetails() async {
    return await FirebaseFirestore.instance.collection('Employee').snapshots();
  }

  Future<void> updateEmployeeDetails(
      String id, Map<String, dynamic> updateInfo) async {
    return await FirebaseFirestore.instance
        .collection('Employee')
        .doc(id)
        .update(updateInfo);
  }

  Future<void> deleteEmployeeDetails(String id) async {
    return await FirebaseFirestore.instance
        .collection('Employee')
        .doc(id)
        .delete();
  }
}
