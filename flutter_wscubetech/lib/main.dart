import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var arrNames = [
      'Rugved',
      'Jidyesh',
      'Hake',
      'Karade',
      'Bhagat',
      'Nitesh',
      'wani',
      'gotya',
      'abhya',
      'sam',
      'tom',
      'ram'
    ];
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Hello'),
        ),
        body: ListView.separated(
            itemBuilder: (context, index) {
              return ListTile(
                leading: Text('${index + 1}'),
                title: Text(arrNames[index]),
                subtitle: Text('Number'),
                trailing: Icon(Icons.add),
              );
            },
            itemCount: arrNames.length,
            separatorBuilder: (context, index) {
              return const Divider(
                height: 20,
                thickness: 1,
              );
            }),
      ),
    );
    /*
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.orange,
          title: const Text('Hello Flutter'),
        ),
        body: ListView.separated(
          itemBuilder: (context, index) {
            return Text(
              arrNames[index],
              style: const TextStyle(fontSize: 21, fontWeight: FontWeight.w500),
            );
          },
          itemCount: arrNames.length,
          separatorBuilder: (context, index) {
            return const Divider(
              height: 100,
              thickness: 30,
            );
          },
        ),
        */
    /*body: Center(
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "One",
                  style: TextStyle(fontSize: 21, fontWeight: FontWeight.w500),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Two",
                  style: TextStyle(fontSize: 21, fontWeight: FontWeight.w500),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Three",
                  style: TextStyle(fontSize: 21, fontWeight: FontWeight.w500),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Four",
                  style: TextStyle(fontSize: 21, fontWeight: FontWeight.w500),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Five",
                  style: TextStyle(fontSize: 21, fontWeight: FontWeight.w500),
                ),
              ),
            ],
          ),
        ),*/
    /*body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 11),
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        Container(
                          margin: EdgeInsets.only(right: 11),
                          height: 200,
                          width: 200,
                          color: Colors.lightGreen,
                        ),
                        Container(
                          margin: EdgeInsets.only(right: 11),
                          height: 200,
                          width: 200,
                          color: Colors.red,
                        ),
                        Container(
                          margin: EdgeInsets.only(right: 11),
                          height: 200,
                          width: 200,
                          color: Colors.black,
                        ),
                        Container(
                          margin: EdgeInsets.only(right: 11),
                          height: 200,
                          width: 200,
                          color: Colors.blue,
                        ),
                        Container(
                          margin: EdgeInsets.only(right: 11),
                          height: 200,
                          width: 200,
                          color: Colors.orange,
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 11),
                  height: 200,
                  color: Colors.orange,
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 11),
                  height: 200,
                  color: Colors.blue,
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 11),
                  height: 200,
                  color: Colors.grey,
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 11),
                  height: 200,
                  color: Colors.purple,
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 11),
                  height: 200,
                  color: Colors.red,
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 11),
                  height: 200,
                  color: Colors.yellow,
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 11),
                  height: 200,
                  color: Colors.black,
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 11),
                  height: 200,
                  color: Colors.pink,
                )
              ],
            ),
          ),
        ),*/
    /*body: 
          Container(
            height: 300,
            width: 300,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  "A",
                  style: TextStyle(fontSize: 50),
                ),
                Text("B", style: TextStyle(fontSize: 50)),
                Text("C", style: TextStyle(fontSize: 50)),
                Text("D", style: TextStyle(fontSize: 50)),
                Text("E", style: TextStyle(fontSize: 50)),
              ],
            ),
          )*/
    /*Center(
            child: Text(
          "hello rugved",
          style: TextStyle(fontSize: 50,),
        )*/
    /* TextButton(
            child: Text("click here!!"),
            onPressed: () {
              print("text button tapped");
            },
            onLongPress: () {
              print("long press");
            },
          ) */
    /* ElevatedButton(
            child: Text("Elevated button"),
            onPressed: () {
              print("elevated pressed");
            },
          ) */
    /*OutlinedButton(
            onPressed: () {
              print("pressed outlined button");
            },
            child: Center(
              child: Text(
                "Outlined button",
                style: const TextStyle(fontSize: 30),
              ),
            ),
          ),*/
  }
}
