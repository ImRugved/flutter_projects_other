import 'package:flutter/material.dart';

class MenuPage extends StatefulWidget {
  const MenuPage({super.key});

  @override
  State<MenuPage> createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 18, 62, 128),
        title: Image.asset(
          'assets/images/cafelive.png',
          height: 60,
          width: 200,
        ),
        centerTitle: true,
        actions: [Icon(Icons.mail_outline_rounded)],
      ),
    );
  }
}
