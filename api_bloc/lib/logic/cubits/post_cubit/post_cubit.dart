import 'package:api_bloc/data/models/post_model.dart';
import 'package:api_bloc/data/repositories/post_repository.dart';
import 'package:api_bloc/logic/cubits/post_cubit/post_state.dart';
import 'package:dio/dio.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

class PostCubit extends Cubit<PostState> {
  PostCubit() : super(PostLoadingState()) {
    fetchPosts();
  }
  PostRepository postRepository = PostRepository();
  void fetchPosts() async {
    try {
      List<PostModel> posts = await postRepository.fetchPost();
      emit(PostLoadedState(posts));
    } on DioError catch (ex) {
      emit(PostErrorState(ex.message.toString()));
    }
  }
}
