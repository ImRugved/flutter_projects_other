import 'dart:math';

import 'package:api_bloc/data/models/post_model.dart';
import 'package:api_bloc/data/repositories/api/api.dart';
import 'package:dio/dio.dart';

class PostRepository {
  API api = API();
  Future<List<PostModel>> fetchPost() async {
    try {
      Response response = await api.sentRequest.get('/posts');
      List<dynamic> postMaps = response.data;
      return postMaps.map((postMap) => PostModel.fromJson(postMap)).toList();
    } catch (ex) {
      throw ex;
    }
  }
}
