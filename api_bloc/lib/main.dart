import 'dart:math';

import 'package:api_bloc/data/models/post_model.dart';
import 'package:api_bloc/data/repositories/post_repository.dart';
import 'package:api_bloc/logic/cubits/post_cubit/post_cubit.dart';
import 'package:api_bloc/presentation/screens/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // PostRepository postRepository = PostRepository();
  // List<PostModel> postModels = await postRepository.fetchPost();
  // log(postModels.toString() as num);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => PostCubit(),
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: HomeScreen(),
      ),
    );
  }
}
