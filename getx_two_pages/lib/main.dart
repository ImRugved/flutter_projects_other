import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:new_app/firstscreen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return const GetMaterialApp(
      title: "My Flutter Applicaton",
      debugShowCheckedModeBanner: false,
    home: FirstScreen(),
    );
  }
}
