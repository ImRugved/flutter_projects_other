import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:new_app/secondscreen.dart';

class FirstScreen extends StatelessWidget {
  const FirstScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('First Screen'),
        backgroundColor: Colors.blue,
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Get.to(SecondScreen());
          },
          child: const Text('Go to second screen'),
        ),
      ),
    );
  }
}
