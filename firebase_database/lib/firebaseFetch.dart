import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class StudentData extends StatefulWidget {
  const StudentData({super.key, this.snapshot, this.index});
  final QuerySnapshot? snapshot;
  final int? index;

  @override
  State<StudentData> createState() => _StudentDataState();
}

class _StudentDataState extends State<StudentData> {
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();

  var firestoreDB =
      FirebaseFirestore.instance.collection('student').snapshots();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Fetch Data From Firebase'),
      ),
      body: StreamBuilder(
        stream: firestoreDB,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return const CircularProgressIndicator();
          }
          return ListView.builder(
            itemCount: snapshot.data!.docs.length,
            itemBuilder: (context, index) {
              return CustomCard(
                snapshot: snapshot.data,
                index: index,
              );
            },
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _showDialog(context);
        },
        child: const Text('Add'),
      ),
    );
  }

  void _showDialog(BuildContext context) async {
    await showDialog(
      context: context,
      builder: (context) => AlertDialog(
        contentPadding: const EdgeInsets.all(10),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text('Please Fill out the form'),
            const SizedBox(height: 10),
            TextField(
              autofocus: true,
              autocorrect: true,
              decoration: const InputDecoration(
                labelText: 'Add name',
              ),
              controller: nameController,
            ),
            const SizedBox(height: 10),
            TextField(
              autofocus: true,
              autocorrect: true,
              decoration: const InputDecoration(
                labelText: 'Add email',
              ),
              controller: emailController,
            ),
          ],
        ),
        actions: [
          TextButton(
            onPressed: () {
              if (nameController.text.trim().isNotEmpty &&
                  emailController.text.trim().isNotEmpty) {
                FirebaseFirestore.instance.collection('student').add({
                  'name': nameController.text,
                  'email': emailController.text,
                }).then((value) {
                  nameController.clear();
                  emailController.clear();
                  Navigator.of(context).pop();
                });
              }
            },
            child: const Text('save'),
          ),
          ElevatedButton(
            onPressed: () {
              nameController.clear();
              emailController.clear();
              Navigator.of(context).pop();
            },
            child: const Text('cancel'),
          ),
        ],
      ),
    );
  }
}

class CustomCard extends StatelessWidget {
  const CustomCard({super.key, this.snapshot, this.index});
  final QuerySnapshot? snapshot;
  final int? index;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Card(
        elevation: 9,
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ListTile(
                title: Text(
                  snapshot!.docs[index!]['name'],
                  style: const TextStyle(fontWeight: FontWeight.bold),
                ),
                subtitle: Text(
                  snapshot!.docs[index!]['email'],
                  style: TextStyle(color: Colors.grey[700]),
                ),
                leading: CircleAvatar(
                  radius: 30,
                  child: Text(
                    snapshot!.docs[index!]['email']
                        .substring(0, 1)
                        .toUpperCase(),
                    style: const TextStyle(
                        fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              const SizedBox(height: 10),
            ],
          ),
        ),
      ),
    );
  }
}
