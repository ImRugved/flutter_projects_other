import 'package:demo_app/screens/second_screen.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('app bar'),
          backgroundColor: Colors.red,
        ),
        body: Center(
          child: Container(
            decoration:
                const BoxDecoration(shape: BoxShape.circle, color: Colors.amber),
            //color: Colors.amber,
            height: 200,
            width: 200,
            child: Center(
              child: ElevatedButton(
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.green)),
                onPressed: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => const SecondScreen()));
                },
                child: const Text("go to next screen"),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
