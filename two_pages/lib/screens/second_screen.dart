import 'package:flutter/material.dart';

class SecondScreen extends StatelessWidget {
  const SecondScreen({super.key});
  @override 
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(),
        body: Center(
          child: Container(
            height: 200,
            width: 200,
            decoration:
                const BoxDecoration(shape: BoxShape.circle, color: Colors.red),
                
            child: Center(
              child: ElevatedButton(
                child: const Text('go back'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
          ),
        ),
      ),
    );
  }
}
