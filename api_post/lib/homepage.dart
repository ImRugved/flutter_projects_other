import 'package:api_post/model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

Future<DataModel?> submitData(String name, String job) async {
  var response = await http.post(Uri.https('reqres.in', 'api/users'),
      body: {'name': name, 'job': job});
  var data = response.body;
  print(data);
  if (response.statusCode == 201) {
    String responseString = response.body;
    dataModelFromJson(responseString);
  } else
    return null;
}

class _HomePageState extends State<HomePage> {
  DataModel? _dataModel;
  TextEditingController nameController = TextEditingController();
  TextEditingController jobController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          'Http Post api',
        ),
      ),
      body: Container(
        padding: const EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Column(
            children: [
              TextField(
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: "Enter Your Name ",
                ),
                controller: nameController,
              ),
              const SizedBox(
                height: 20,
              ),
              TextField(
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: "Enter Job Name ",
                ),
                controller: jobController,
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                onPressed: () async {
                  String name = nameController.text;
                  String job = jobController.text;
                  DataModel? data = await submitData(name, job);
                  setState(() {
                    _dataModel = data;
                  });
                },
                child: const Text('Submit'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
