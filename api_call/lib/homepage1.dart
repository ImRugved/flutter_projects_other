import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Homepage1 extends StatefulWidget {
  @override
  State<Homepage1> createState() => _Homepage1State();
}

class _Homepage1State extends State<Homepage1> {
  List<dynamic> users = [];

  void fetchData() async {
    const apiUrl = 'https://reqres.in/api/users?page=1&per_page=12';

    final uri = Uri.parse(apiUrl);
    final response = await http.get(uri);

    if (response.statusCode == 200) {
      setState(
        () {
          users = jsonDecode(response.body)['data'];
        },
      );
    } else {
      // Handle error, for example, show an error message
      print('Failed to load data: ${response.statusCode}');
    }
    print(users);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('API Call'),
      ),
      body: Column(
        children: [
          users.isEmpty
              ? Container(
                  margin: EdgeInsets.all(20),
                  padding: EdgeInsets.only(top: 350),
                  child: Text(
                    'No data available. Refresh to get the data.',
                    style: TextStyle(fontSize: 25),
                  ),
                )
              : Container(),
          Expanded(
            child: ListView.builder(
              itemCount: users.length,
              itemBuilder: (context, index) {
                return Card(
                  child: ListTile(
                    leading: CircleAvatar(
                      backgroundImage: NetworkImage(
                        users[index]['avatar'],
                      ),
                    ),
                    title: Text(
                      '${users[index]['first_name']} ${users[index]['last_name']}',
                    ),
                    subtitle: Text(
                      users[index]['email'],
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.green,
        onPressed: () {
          fetchData();
        },
        child: Icon(Icons.refresh),
      ),
    );
  }
}
