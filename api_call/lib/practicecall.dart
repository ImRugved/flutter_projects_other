import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class PracticeCall extends StatefulWidget {
  @override
  State<PracticeCall> createState() => _PracticeCallState();
}

class _PracticeCallState extends State<PracticeCall> {
  List data = [];

  void getData() async {
    final url = await Uri.parse('https://jsonplaceholder.typicode.com/photos');

    final response = await http.get(url);
    if (response.statusCode == 200) {
      setState(() {
        data = jsonDecode(response.body);
      });
    } else {
      print("Failed to load data : ${response.statusCode}");
    }
    print(data);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Practice API Call'),
      ),
      body: ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) => ListTile(
          leading: CircleAvatar(
            backgroundImage: NetworkImage('${data[index]['thumbnailUrl']}'),
          ),
          title: Text('${data[index]['title']}'),
          subtitle: Text('${data[index]['url']}'),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          getData();
        },
        child: const Icon(
          Icons.add,
        ),
      ),
    );
  }
}
