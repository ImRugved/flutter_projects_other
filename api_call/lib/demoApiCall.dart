import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

class DemoApiCall extends StatefulWidget {
  const DemoApiCall({super.key});

  @override
  State<DemoApiCall> createState() => _DemoApiCallState();
}

class _DemoApiCallState extends State<DemoApiCall> {
  List<dynamic> users = [];
  void fetchData() async {
    final url = Uri.parse('https://reqres.in/api/users?page=1&per_page=12');
    final response = await http.get(url);
    if (response.statusCode == 200) {
      setState(() {
        users = jsonDecode(response.body)['data'];
      });
    } else {
      print("Failed to load data : ${response.statusCode}");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[900],
        appBar: AppBar(
          backgroundColor: Colors.purple[200],
          centerTitle: true,
          title: const Text(
            'API Call ',
            style: TextStyle(fontSize: 35),
          ),
        ),
        drawer: const DrawerButton(),
        body: Column(
          children: [
            users.isEmpty
                ? Container(
                    padding: const EdgeInsets.only(top: 350),
                    child: const Center(
                      child: Text(
                        textAlign: TextAlign.center,
                        'No data available, Please Refresh',
                        style: TextStyle(
                          fontSize: 25,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  )
                : Container(),
            Expanded(
              child: ListView.builder(
                itemCount: users.length,
                itemBuilder: (context, index) => Card(
                  child: ListTile(
                    leading: CircleAvatar(
                      radius: 25,
                      backgroundImage: NetworkImage(
                        users[index]['avatar'],
                      ),
                    ),
                    title: Row(
                      children: [
                        Text(
                          '${index + 1}. ',
                          style: const TextStyle(
                            fontSize: 25,
                          ),
                        ),
                        Text(
                          '${users[index]['first_name']} ${users[index]['last_name']}',
                          style: const TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                    subtitle: Text(
                      '${users[index]['email']}',
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.purple[200],
          onPressed: () {
            fetchData();
          },
          child: const Icon(
            Icons.refresh,
          ),
        ));
  }
}

//////////////////////////////////////////////////

// import 'dart:convert';

// import 'package:flutter/material.dart';

// import 'package:http/http.dart' as http;

// class DemoApiCall extends StatefulWidget {
//   const DemoApiCall({super.key});

//   @override
//   State<DemoApiCall> createState() => _DemoApiCallState();
// }

// class _DemoApiCallState extends State<DemoApiCall> {
//   List<dynamic> users = [];

//   void fetchData() async {
//     final url = Uri.parse('https://jsonplaceholder.typicode.com/photos');

//     final response = await http.get(url);

//     if (response.statusCode == 200) {
//       setState(() {
//         users = jsonDecode(response.body);
//       });
//     } else {
//       print('Failed to fetch data ${response.statusCode}');
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       //backgroundColor: Colors.yellow[300],
//       appBar: AppBar(
//         backgroundColor: Colors.purple,
//         centerTitle: true,
//         title: const Text('Demo api data fetch'),
//       ),
//       body: Column(
//         children: [
//           Expanded(
//             child: ListView.builder(
//               itemCount: users.length,
//               itemBuilder: (context, index) {
//                 return ListTile(
//                   contentPadding: EdgeInsets.all(10),
//                   leading: CircleAvatar(
//                     backgroundImage: NetworkImage(
//                       users[index]['thumbnailUrl'],
//                     ),
//                   ),
//                   title: Text(
//                     '${users[index]['title']}',
//                   ),
//                 );
//               },
//             ),
//           ),
//           const SizedBox(
//             height: 20,
//           ),
//         ],
//       ),
//       floatingActionButton: FloatingActionButton(
//         onPressed: () {
//           fetchData();
//         },
//         child: const Icon(Icons.refresh),
//       ),
//     );
//   }
// }
