import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class HomePage4 extends StatefulWidget {
  const HomePage4({super.key});

  @override
  State<HomePage4> createState() => _HomePage4State();
}

class _HomePage4State extends State<HomePage4> {
  String _response = '';
  Future<void> fetchData() async {
    try {
      final url =
          Uri.parse('https://api.coindesk.com/v1/bpi/currentprice.json');
      final response = await http.get(url);

      if (response.statusCode == 200) {
        setState(() {
          _response = response.body;
        });
      } else {
        setState(() {
          _response = 'Failed to load data!';
        });
      }
    } catch (error) {
      setState(() {
        _response = "Couldn't connect to the internet! $error";
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('User Data from API'),
      ),
      body: Padding(
        padding: EdgeInsets.all(16),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                onPressed: () {
                  fetchData();
                },
                child: Text('Make Req'),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                'Response : ',
                style: TextStyle(fontSize: 18),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                _response,
                style: TextStyle(fontSize: 16),
              )
            ],
          ),
        ),
      ),
    );
  }
}
