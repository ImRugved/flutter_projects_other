import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;

class HomePage3 extends StatefulWidget {
  @override
  State<HomePage3> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage3> {
  List<dynamic> users = [];
  void fetchData() async {
    final apiUrl = 'https://reqres.in/api/users?page=1&per_page=12';
    final uri = Uri.parse(apiUrl);
    final respose = await http.get(uri);
    if (respose.statusCode == 200) {
      setState(() {
        users = jsonDecode(respose.body)['data'];
      });
    } else {
      print('failed to load the data : ${respose.statusCode}');
    }
    print(users);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellowAccent,
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: const Text('API Call'),
      ),
      body: Column(
        children: [
          users.isEmpty
              ? Container(
                  margin: const EdgeInsets.all(20),
                  padding: const EdgeInsets.only(top: 350),
                  child: const Text(
                    '           No data available. Refresh to get the data.',
                    style: TextStyle(fontSize: 25),
                  ),
                )
              : Container(),
          Expanded(
            child: ListView.builder(
                itemCount: users.length,
                itemBuilder: (context, index) {
                  return Card(
                    child: ListTile(
                      leading: CircleAvatar(
                        backgroundImage: NetworkImage(
                          users[index]['avatar'],
                        ),
                      ),
                      title: Text(
                        '${users[index]['first_name']},${users[index]['last_name']}',
                      ),
                      subtitle: Text(
                        '${users[index]['email']} ',
                      ),
                    ),
                  );
                }),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.purple,
        onPressed: () {
          fetchData();
        },
        child: const Icon(Icons.refresh),
      ),
    );
  }
}
