import 'addstudentpage.dart';
import 'liststudentpage.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.purple[300],
      appBar: AppBar(
        backgroundColor: Colors.yellowAccent,
        title: const Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Flutter',
              style: TextStyle(
                fontSize: 30.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Text(
              'CRUD',
              style: TextStyle(
                fontSize: 30.0,
                color: Colors.deepPurple,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
      body: ListStudentPage(),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.yellowAccent,
        tooltip: 'Add New Student',
        onPressed: () => {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => AddStudentPage(),
            ),
          ),
        },
        child: const Text(
          'Add',
          style: TextStyle(
            fontSize: 20.0,
          ),
        ),
      ),
    );
  }
}
