import 'package:flutter/material.dart';
import 'package:ui_1/myStyle.dart';

class SinglePost extends StatelessWidget {
  const SinglePost({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          decoration: const BoxDecoration(
            //color: Colors.red,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(50.0),
              topLeft: Radius.circular(50.0),
            ),
          ),
          margin: const EdgeInsets.only(
            left: 30.0,
          ),
          height: 150,
          width: double.infinity,
          child: ClipRRect(
            borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(50.0),
              topLeft: Radius.circular(50.0),
            ),
            child: Image.asset(
              'assets/image.jpg',
              fit: BoxFit.cover,
            ),
          ),
        ),
        const SizedBox(
          height: 8,
        ),
        Container(
          margin: const EdgeInsets.only(
            left: 80.0,
            right: 5,
            bottom: 30,
          ),
          child: const Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Subcribe to flutter today', style: postText),
              Row(
                children: [
                  Icon(
                    Icons.comment_outlined,
                    color: Colors.white,
                    size: 16.0,
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    '15',
                    style: postText,
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  Icon(
                    Icons.favorite_border,
                    color: Colors.white,
                    size: 16.0,
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    '150k',
                    style: postText,
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
