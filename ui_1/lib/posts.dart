import 'package:flutter/material.dart';
import 'package:ui_1/singlePost.dart';

class MyPosts extends StatelessWidget {
  const MyPosts({super.key});

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.only(top: 370),
      child: Column(
        children: [
          Column(
            children: [
              SinglePost(),
              SinglePost(),
              SinglePost(),
              SinglePost(),
              SinglePost(),
              SinglePost(),
            ],
          ),
        ],
      ),
    );
  }
}
