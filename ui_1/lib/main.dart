import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ui_1/appBar.dart';
import 'package:ui_1/myStyle.dart';
import 'package:ui_1/posts.dart';
import 'package:ui_1/profile.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SafeArea(
        child: Scaffold(
          backgroundColor: mainColor,
          body: ListView(
            children: const [
              Stack(
                children: [
                  MyPosts(),
                  Profile(),
                  MyAppBar(),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
